<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
});

Auth::routes([
    'register'  => false,
    'reset'  => true,
    'verify'  => false,
]);

Route::get('/home', 'HomeController@index')->name('home');



Route::prefix('admin')->namespace('admin')->name('admin.')->middleware('can:can-admin')->group(function(){
    Route::resource('users','UsersController')->except(['create','store']);
});



Route::prefix('admin')->namespace('admin')->name('admin.')->middleware('can:can-admin')->group(function(){
    Route::resource('companies','CompaniesController')->except(['create','store']);
});



Route::prefix('admin')->namespace('admin')->name('admin.')->middleware('can:can-admin')->group(function(){
    Route::resource('categories','CategoriesController');
});


Route::prefix('admin')->namespace('admin')->name('admin.')->middleware('can:can-admin')->group(function(){
    Route::resource('skills','SkillsController');
});



Route::prefix('admin')->namespace('admin')->name('admin.')->middleware('can:can-admin')->group(function(){
    Route::resource('job-offers','JobOffersController')->except(['create','store']);
});

