<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
*/

Route::group( ['middleware' => 'api' ], function()
{
    Route::get('users','UsersController@index'); // le api devi richiamarle con PostMan http://localhost:8000/api/users devi riscriverti l'URL
    Route::get('users/{user}','UsersController@show');


    Route::get('delete-user/{user}','UsersController@destroy');
    Route::get('restore-user/{user}','UsersController@restore');


    Route::post('users-by-skill','UsersController@usersBySkill');
    Route::get('all-skills','UsersController@allSkills');
    Route::post('job-offers-by-skill','jobOffersController@jobOffersBySkill');

    Route::get('companies','CompaniesController@index');
    Route::get('company-categories','CompaniesController@allCategories');
    Route::get('companies/{company}','CompaniesController@show');

    Route::post('job-offers','jobOffersController@store');
} );




Route::group( ['middleware' => 'api' ], function()
{
    Route::post('register-user','AuthController@registerUser');
    Route::post('register-company','AuthController@registerCompany');
    Route::post('user-login','AuthController@userLogin');
    Route::post('company-login','AuthController@companyLogin');
} );

Route::group( ['middleware' => 'auth:api' ], function() //auth vuol dire che sei loggato, il logout e il profilo l ovedono solo quelli loggati
{
    Route::post('my-profile','AuthController@myProfile');
    Route::post('logout','AuthController@logout');
    Route::post('user-skill-add','UsersController@addSkill');
    Route::post('user-skill-remove','UsersController@removeSkill');
    Route::post('my-skills','UsersController@mySkill');
    Route::post('upload-logo','CompaniesController@updLoadLogo');
    Route::post('upload-user-photo','UsersController@updLoadLogo');
} );
