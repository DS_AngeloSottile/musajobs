@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-12">
            <h1> {{$company->name}} </h1>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <ul class="list-group">

                @forelse( $company->jobOffers as $job_offer)
                    <li class="list-group-item">{{$job_offer->role}}</li>
                    <hr>
                    @foreach ($job_offer->statusHistory as $history_item)
                    <div class="d-flex justify-content-between">
                        <div>
                            {{$history_item->label}}
                        </div>
                        <div class="d-flex flex-column">
                            <div>
                                <strong>DA:</strong>
                                <span>{{$history_item->pivot->from}}</span>
                            </div>
                            <div>
                                <strong>A:</strong>
                                <span>{{$history_item->pivot->to}}</span>
                            </div>
                        </div>
                    </div>
                    @endforeach
                @empty
                    <li class="list-group-item active">Nessuan offerta di</li>
                @endforelse
            </ul>
        </div>
    </div>
</div>

@endsection
