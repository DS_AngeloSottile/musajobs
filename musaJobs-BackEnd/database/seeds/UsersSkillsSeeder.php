<?php

use App\Models\skill;
use App\Models\UserDetails;
use Illuminate\Database\Seeder;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

class UsersSkillsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        $user_details = UserDetails::all();
        $skills = skill::get()->pluck('id')->toArray();

        foreach($user_details as $user)
        {
            try
            {
                DB::beginTransaction();

                $random_skills = Arr::random($skills,$faker->numberBetween(5,10)); // prendi da skills e quanti ne vuoi prendere

                for( $i = 0; $i < count($random_skills); $i++)
                {

                    $random_level = $faker->randomElement( [ 1, 1.5, 2, 2.5, 3, 3.5, 4, 4.5, 5 ] );

                    $random_experience = $faker->numberBetween(1,10);

                    $user->skills()->attach($random_skills[$i], ['level' => $random_level, 'experience_year' => $random_experience]);

                }

                DB::commit();

            }
            catch(\Exception $e)
            {
                DB::rollback();

            }

        }
    }
}
