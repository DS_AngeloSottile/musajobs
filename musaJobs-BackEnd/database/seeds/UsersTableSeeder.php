<?php

use App\Models\Role;
use App\Models\User;
use App\Models\UserDetails;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->createAdmins();
        $this->createSomeUsers(20);
    }

    private function createAdmins()
    {
        $admin_role = Role::where('code','admin')->first();

        $user_data =
        [
            'first_name'           =>'Angelo',
            'last_name'            =>'Sottile',
            'email'                =>'angelosottile@hotmail.it',
            'username'             =>'drakeserra',
            'password'             =>Hash::make('password'),
            'privacy'              =>1,
            'role_id'              =>$admin_role->id,
            'email_verified_at'    =>Carbon::now(),
        ];

        User::create($user_data);
    }

    private function createSomeUsers($qty)
    {
        $user_role = Role::where('code','user')->first();
        $faker = Faker\Factory::create('it_IT');
        $now=Carbon::now();
        for( $i = 0; $i < $qty; $i++ )
        {
            try // tutto questo blocco lo metti nel try catch per evitare problemi, è possibile che faker mette 2 volte la stessa email e tu hai messo unsigned e quindi scoppia tutto
            {
                DB::beginTransaction();


                $user_data =
                [
                    'first_name'           =>$faker->firstName,
                    'last_name'            =>$faker->lastName,
                    'email'                =>$faker->email,
                    'username'             =>$faker->userName,
                    'password'             =>Hash::make('password'),
                    'privacy'              =>1,
                    'role_id'              =>$user_role->id,
                    'email_verified_at'    =>$now,
                    // 'details_id'           =>$user_details->id,
                    // 'details_type'         =>'App\Models\UserDetails'
                ];
                $user = User::create($user_data);

                $user_details_data =
                [
                    'bio'               =>$faker->text(300),
                    'birth_date'        =>null,
                    'phone'             =>null,
                    'gender'            =>null,
                    'city'              =>null,
                ];
                $user_details = UserDetails::create($user_details_data);


                $user->details()->associate($user_details)->save();

                DB::commit();
            }
            catch(\Exception $e)
            {
                DB::rollback();
            }

        }
    }
}
