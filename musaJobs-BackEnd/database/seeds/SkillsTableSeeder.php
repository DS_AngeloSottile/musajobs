<?php

use App\Models\skill;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class SkillsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now=Carbon::now();
        $skills=[];
        for( $i=0; $i<30; $i++)
        {
            $skills[] =
            [
                'label'         => 'Skill' . $i,
                'code'          => 'skill-' . $i,
                'created_at'    => $now,
                'updated_at'    => $now,
            ];
        };
        Skill::insert($skills);
    }
}
