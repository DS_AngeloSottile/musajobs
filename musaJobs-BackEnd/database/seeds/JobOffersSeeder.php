<?php

use App\Models\Company;
use App\Models\JobOffer;
use App\Models\skill;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

class JobOffersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        $now = Carbon::now();

        $companies = Company::all();
        $skills = skill::get()->pluck('id')->toArray();

        foreach($companies as $company)
        {
            try
            {
                DB::beginTransaction();
                $random_job_offers_count = $faker->numberBetween(1,10);
                for( $joc = 0; $joc < $random_job_offers_count; $joc++)
                {

                    $job_offer_data =
                    [
                        'role'                  => $faker->sentence(3),
                        'description'           => $faker->text(200),
                        'status'                => 1,
                        'valid_from'            => $now,
                        'valid_to'              => null,
                    ];

                    $job_offer = $company->jobOffers()->create($job_offer_data);

                    $job_offer->statusHistory()->attach(1,['last' => true, 'from' => $now, 'to' => null] );

                    $randomm_skills = Arr::random( $skills, $faker->numberBetween(3,7) );

                    for($rs = 0; $rs < count($randomm_skills); $rs++)
                    {
                        $random_min_level = $faker->randomElement( [ 1, 1.5, 2, 2.5, 3, 3.5, 4, 4.5, 5 ] );

                        $max_level_array = [];
                        for($ml = $random_min_level; $ml <= 5; $ml += 0.5 )
                        {
                            $max_level_array[] = $ml;
                        }
                        $random_max_level = $faker->randomElement( $max_level_array );

                        $random_min_exp = $faker->numberBetween(1,10);

                        $extra_data =
                        [
                            'min_level'             => $random_min_level,
                            'max_level'             => $random_max_level,
                            'min_experience_years'  => $random_min_exp,
                        ];
                        $job_offer->skills()->attach($randomm_skills[$rs], $extra_data );
                    }

                }

                DB::commit();
            } catch(\Exception $e)
            {
                DB::rollback();
            }

        }
    }
}
