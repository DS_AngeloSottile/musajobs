<?php

use App\Models\Category;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class CompanyCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now=Carbon::now();
        $categories=[];
        for( $i=0; $i<15; $i++)
        {
            $categories[] =
            [
                'label'         => 'Category' . $i,
                'code'          => 'category-' . $i,
                'created_at'    => $now,
                'updated_at'    => $now,
            ];
        };
        Category::insert($categories);
    }
}
