<?php

use App\Models\JobOffer;
use App\Models\User;
use App\Models\UserDetails;
use Illuminate\Database\Seeder;

class UserJobOffersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = UserDetails::with('skills')->get();

        foreach($users as $user)
        {
            foreach($user->skills as $skill)
            {
                $skill_id = $skill->id;
                $skill_level = $skill->pivot->level;
                $skill_exp = $skill->pivot->experience_year;

                if($skill_level > 2)
                {
                    $job_offers = JobOffer::whereHas(
                        'skills', function($q) use ($skill_id, $skill_level, $skill_exp)
                        {
                            $q->where('skill_id',$skill_id); // se la skill dell'offerta di lavoro è uguale a quella dell'utente
                            $q->where('min_level','>=',$skill_level);
                            $q->where('max_level','<=',$skill_level);
                            $q->where('min_experience_years','>=',$skill_exp);

                        }
                    )->get();
                    $job_offers_ids = $job_offers->pluck('id')->toArray();

                    $user->jobOffers()->attach( $job_offers_ids);


                }
            }
        }
    }
}
