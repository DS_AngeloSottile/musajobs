<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Eloquent\SoftDeletes;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('email')->unique();
            $table->string('username')->unique();
            $table->string('password');
            $table->rememberToken();
            $table->boolean('privacy')->default(false);
            $table->string('details_type')->nullable()->default(null); //devi chiamarle con details davanti perchè il nome della relazione in questo campo ci sarà il percorso alla relazione
            $table->bigInteger('details_id')->unsigned()->nullable()->default(null); //devi chiamarle con details davanti perchè il nome della relazione
            $table->integer('role_id')->unsigned();
            $table->timestamp('email_verified_at')->nullable();
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable()->default(null);
            $table->timestamp('last_login')->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
