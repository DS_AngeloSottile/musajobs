<?php

namespace App\Console\Commands;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Console\Command;

class NotActiveUsersNotifications extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:not-active {role}' ;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Utenti che non si sono registrati';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // ovvero che il vettore ARGS alla cella 'role' avrà quello che scrivi   user:not-active user  allora  $args['role] => 'user'
        $args = $this->arguments(); // user:not-active {role} con arguments leggi il valore tra parentesi graffe e lo metti in una CELLA di args
        //$role = $this->argument('role');
        $now = Carbon::now();

        $limit =$now->subDays( 14 ); // meno 14 giorni da oggi
        $users = User::notActive()->byType($args['role'])->where('created_at','<',$limit)->get();

        // dd($users->count());
        foreach($users as $user)
        {
            $user->forceDelete();
        } // quanti elementi ti ha restituito  $user->count()
    }
}
