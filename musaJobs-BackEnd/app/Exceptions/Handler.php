<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception  $exception
     * @return void
     *
     * @throws \Exception
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Exception
     */
    public function render($request, Exception $exception) // quando invi una pagina tramite postman
    {
        /* Questi sono i primi messagi che vedi, tu devi vedere l'errore in exception
            "message": "",
            "exception": "Symfony\\Component\\HttpKernel\\Exception\\NotFoundHttpException",
            "file": "C:\\xampp\\htdocs\\studiophp\\musa-jobs\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\RouteCollection.php",
            "line": 179,
            "trace": [
        */

        if( $request->expectsJson() )
        {
            if( $exception instanceof ModelNotFoundException ) // se l'errore è ModelNotFoundException allora mandi quel messaggi di errore
            {
                return response()->json ( [ 'errors' => true, 'message' => 'Model not found' ] );
            }
            else if( $exception instanceof NotFoundHttpException  )
            {
                return response()->json ( [ 'errors' => true, 'message' => 'End point not valid' ] );
            }

        }

        return parent::render($request, $exception);
    }
}
