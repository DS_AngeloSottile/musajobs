<?php

namespace App\Http\Controllers;

use App\Http\Requests\CompanyLogoRequest;
use App\Http\Requests\UsersBySkillRequest;
use App\Http\Requests\UserSkillManagment;
use App\Models\skill;
use App\Models\User;
use App\Models\UserDetails;
use App\Traits\ApiTrait;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\JsonResponse as HttpFoundationJsonResponse;

class UsersController extends Controller
{
    use ApiTrait;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $queryString = request()->query();

        if(isset($queryString['sort']))
        {
            $sort = $queryString['sort'];
        }
        else
        {
            $sort = null;
        }


        if(isset($queryString['page_size']))
        {
            $page_size = $queryString['page_size'];
        }
        else
        {
            $page_size = 5;
        }


        $users_query = User::byType('user')->with('details.skills');

        if( !is_null($sort))
        {
            $users_query->ordered($sort);
        }

        $users = $users_query->paginate($page_size);
        return response()->json( ['result' => $users, 'code' => 200 ],200 );
    }



    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) // lo show devi gestirlo con l'errore, vai nella cartella exceptions\handler nella funzione render
    {
        $user = User::byType( 'user' )->with( [ 'details','skills' ] )->findOrFail( $id );
        return response()->json( ['result' => $user, 'code' => 200 ],200 );
    }


    public function usersBySkill(UsersBySkillRequest $request)
    {

        $query_inputs = $request->get('skills');

        $users =User::byType('user')
                    ->whereHasMorph('details',
                                        [UserDetails::class],
                                        function($q) use ($query_inputs)
                                        {
                                            foreach($query_inputs as $query_input)
                                            {
                                                $q->whereHas('skills',
                                                                function($q) use($query_input)
                                                                    {
                                                                        $q->where('skill_id',$query_input['id']);
                                                                        $q->where('level','>=',$query_input['min_level']);
                                                                        $q->where('level','<=',$query_input['max_level']);
                                                                        $q->where('experience_year','>=',$query_input['min_exp']);
                                                                    }
                                                            );
                                            }
                                        }
                                    )->with(['details.skills'])->get();

        return response()->json( ['result' => $users, 'code' => JsonResponse::HTTP_OK ],JsonResponse::HTTP_OK );


        /*
        $users = User::byType('user')->with(
            [
                'details'=>function($q) use($skill_id)
                {
                    $q->whereHas('skills', function($q) use($skill_id)
                    {
                        $q->where('skill_id',$skill_id);
                    })->with('skills');
                },
            ] )->get();
        */
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);
        $deleted = $user->delete();

        if($deleted)
        {
            return response()->json( ['result' => 'utente cancellato ocrrettamente', 'code' => JsonResponse::HTTP_NO_CONTENT ],JsonResponse::HTTP_NO_CONTENT );
        }
        else
        {
            return response()->json( ['error' => true,'message' => 'errore cancellazzione utente', 'code' => JsonResponse::HTTP_INTERNAL_SERVER_ERROR ],JsonResponse::HTTP_INTERNAL_SERVER_ERROR );
        }
    }

    public function restore($id)
    {
        $user = User::withTrashed()->findOrFail($id);
        $user_restored = $user->restore();

        if($user_restored)
        {
            return response()->json( ['result' => 'utente ripristinato', 'code' => JsonResponse::HTTP_OK ],JsonResponse::HTTP_OK );
        }
        else
        {
            return response()->json( ['error' => true,'message' => 'errore ripristino utente', 'code' => JsonResponse::HTTP_INTERNAL_SERVER_ERROR ],JsonResponse::HTTP_INTERNAL_SERVER_ERROR );
        }
        dd($user_restored);
    }

    /*
    public function updLoadLogo(Request $request)
    {
        $user = Auth::user();


        $company_logo = 'companies/default.png';

        $user = $user->details;
        dd($user);

        if($company &&  $request->hasFile( 'logo' ))
        {
            $file = $request->file( 'logo' );
            $filename = time(). '.' . $file->getClientOriginalExtension(); // mette un nome generico al file
            $file_path = 'companies/' . $company->id . '/logo';
            $uploaded_file = $file->storeAs( $file_path,$filename,'public');
            $company->logo= $uploaded_file; // php artisan storage:link
            $company->save();

            return $this->successResponse(['logo_url' =>$company->company_logo_full_url],JsonResponse::HTTP_CREATED);
        }
    } */

    public function addSkill( UserSkillManagment $request)
    {
        $user = User::byType('user')->with('details.skills')->findOrFail(Auth::id());

        $extras =
        [
            'level' => $request->lvl,
            'experience_year' => $request->exp_years,
        ];

        try
        {
            $final = $user->details->skills()->sync([$request->skill_id => $extras], false ); // o usi attach come nel seed di usersSkills o usi sync e metti false, così puoi eseguire una query di update anche se hai già inserito la stessa skill
            $user_skills = $user->details->skills()->get();

            $result =
            [
                "user_skills" => $user_skills,
                'message' => "Skill aggiunta con successo",
            ];

            if( count( $final[ 'attached' ] ) > 0 )
            {
                return response()->json( [ "result" => $result, 'code' => JsonResponse::HTTP_CREATED ],JsonResponse::HTTP_CREATED );
            }
            else if ( count( $final[ 'updated' ] ) > 0 )
            {
                return response()->json( ["result" => $result,'code' => JsonResponse::HTTP_OK ],JsonResponse::HTTP_OK );

            }
            else
            {
                return $this->successResponse( "Nessuna skill coninvolta", JsonResponse::HTTP_OK );
            }
        }
        catch( \Exception $e)
        {
            return $this->errorResponse( "Errore salvataggio skill utente", JsonResponse::HTTP_UNPROCESSABLE_ENTITY );
        }

    }

    public function removeSkill(Request $request)
    {
        $user = User::byType('user')->with('details.skills')->findOrFail(Auth::id());

        $final = $user->details->skills()->detach($request->skill_id);
        $user_skills = $user->details->skills()->get();

        $result =
        [
            "user_skills" => $user_skills,
            'message' => "Skill eliminata con successo",
        ];

        if($final === 1)
        {
            return response()->json( [ "result" => $result, 'code' => JsonResponse::HTTP_OK ],JsonResponse::HTTP_OK );
        }
        else
        {
            return response()->json( ["result" => $result,'code' => JsonResponse::HTTP_UNPROCESSABLE_ENTITY ],JsonResponse::HTTP_UNPROCESSABLE_ENTITY );
        }
    }

    public function mySkill(Request $request)
    {
        $user = User::byType('user')->with('details.skills')->findOrFail(Auth::id());

        $result = $user->details->skills()->get();
        return response()->json(
            [
                'result' => $result,
                'code' =>JsonResponse::HTTP_OK,
            ],
            JsonResponse::HTTP_OK,
        );
    }

    public function allSkills( Request $request )
    {
        $skills = skill::get()->all();
        return response()->json( [ 'result' => $skills, 'code' => 200], 200 );
    }

}
