<?php

namespace App\Http\Controllers;

use App\Http\Requests\CompanyRegistrationRequest;
use App\Models\Company;
use App\Models\User;
use App\Models\UserDetails;
use App\Traits\ApiTrait;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Laravel\Passport\Passport;
use Laravel\Passport\Token;
use Laravel\Passport\HasApiTokens;

class AuthController extends Controller
{
    use ApiTrait;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }


    public function registerUser( Request $request)
    {
        $validator = Validator::make($request->all(), // o così o crei la request, è uguale
            [
                'first_name'        => 'required|max:255',
                'last_name'         => 'required|max:255',
                'email'             => 'required|max:255|email|unique:users,email',
                'password'          => 'required|max:255|min:8|alpha_num',
                'confirm_password'  => 'required|same:password',
            ]
        );

        if($validator->fails())
        {
            return response()->json( ['error' => true, 'message' => $validator->errors(), 'status' =>JsonResponse::HTTP_UNPROCESSABLE_ENTITY ], JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
        }

        try
        {
            DB::beginTransaction();
            $username = $request->first_name . $request->last_name;

            $input_data =
            [
                'first_name'            =>$request->first_name,
                'last_name'             =>$request->last_name,
                'email'                 =>$request->email,
                'password'              =>Hash::make($request->password),
                'username'              =>$username,
                'role_id'               =>2,

            ];

            $user = User::create($input_data);

            $user_details_data =
            [
                'bio'               =>'',
                'birth_date'        =>null,
                'phone'             =>null,
                'gender'            =>null,
                'city'              =>null,
            ];

            $user_details = UserDetails::create($user_details_data);

            $user->details()->associate( $user_details )->save();

            $result =
            [
                'token'         => $user->createToken('token')->plainTextToken,
                'userData'      => $user,
            ];


            DB::commit();
             return response()->json( ['result' => $result,'code' => JsonResponse::HTTP_CREATED ],JsonResponse::HTTP_CREATED );
            //return $this->successResponse($user, JsonResponse::HTTP_CREATED );

        }
        catch(\Exception $e)
        {
            Db::rollBack();
            return $this->errorResponse($validator->errors(), JsonResponse::HTTP_INTERNAL_SERVER_ERROR );
            /*
            return response()->json(
                [
                    'error' => 'Errore nelal connessioen al database',
                    'message' => $validator->errors(),
                    'status' =>JsonResponse::HTTP_INTERNAL_SERVER_ERROR
                ],
                JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
            */
        }

    }

    public function registerCompany( Request $request )
    {
        // try
        // {
            // DB::beginTransaction();

            $company_code = '';

            while($company_code === '')
            {
                $random_number = rand();
                $username = 'MJC' . $random_number;
                $user_exist = User::where('username',$username)->first();

                if(is_null($user_exist))
                {
                    $company_code = $username;
                }
            }

            $input_data =
            [
                'first_name'                =>$request->first_name,
                'last_name'                 =>$request->last_name,
                'email'                     =>$request->email,
                'password'                  =>Hash::make($request->password),
                'username'                  =>$company_code,
                'role_id'                   =>3,

            ];

            $user = User::create($input_data);

            $company_data =
            [
                'name'          =>  $request->company_name,
                'description'   => '',
                'city'          => '',
                'website'       => '',
                'logo'          => '',
                'category_id'   => $request->category,
            ];

            $company_details = Company::create($company_data);

            $user->details()->associate( $company_details )->save();

            $result =
            [
                'token'         => $user->createToken('token')->plainTextToken,
                'userData'      => $user,
            ];


           // DB::commit();
             return response()->json( ['result' => $result,'code' => JsonResponse::HTTP_CREATED ],JsonResponse::HTTP_CREATED );
            //return $this->successResponse($user, JsonResponse::HTTP_CREATED );

        /*}
        catch(\Exception $e)
        {
            Db::rollBack();
            return response()->json(
                [
                    'error' => 'Errore nelal connessioen al database',
                    'message' => 'errore registrazione',
                    'status' =>JsonResponse::HTTP_INTERNAL_SERVER_ERROR
                ],
                JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        } */


    }



    public function userLogin( Request $request )
    {
        $validator = Validator::make($request->all(),
            [
                'email'             => 'required|email',
                'password'          => 'required',
            ]
        );

        if($validator->fails())
        {
            return response()->json(
                [
                    'error' => true,
                    'message' => $validator->errors(),
                    'status' =>JsonResponse::HTTP_BAD_REQUEST,
                ],
                JsonResponse::HTTP_BAD_REQUEST,
            );
        }

        if( Auth::attempt( ['email' => $request->email, 'password' => $request->password, 'role_id' => 2 ] ) )
        {
            $user = Auth::user();
            $user->load('details');


            $result =
            [
                'token'         => $user->createToken('token')->plainTextToken,
                'userData'      => $user,
            ];

            return response()->json(
                [
                    'result' => $result,
                    'code' =>JsonResponse::HTTP_OK,
                ],
                JsonResponse::HTTP_OK,
            );

        }
        else
        {
            return response()->json(
                [
                    'error' => true,
                    'message' => 'Login non valido',
                    'status' =>JsonResponse::HTTP_UNAUTHORIZED,
                ],
                JsonResponse::HTTP_UNAUTHORIZED,
            );

        }

    }

    public function companyLogin( Request $request )
    {
        if( Auth::attempt( ['username' => $request->username, 'password' => $request->password, 'role_id' => 3 ] ) )
        {
            $user = Auth::user();
            $user->load('details');

            $result =
            [
                'token'         => $user->createToken('token')->plainTextToken,
                'userData'      => $user,
            ];

            return response()->json(
                [
                    'result' => $result,
                    'code' =>JsonResponse::HTTP_OK,
                ],
                JsonResponse::HTTP_OK,
            );

        }
        else
        {
            return response()->json(
                [
                    'error' => true,
                    'message' => 'Login non valido',
                    'status' =>JsonResponse::HTTP_UNAUTHORIZED,
                ],
                JsonResponse::HTTP_UNAUTHORIZED,
            );

        }

    }



    public function logout( Request $request)
    {
        /*
        $revoked_token_counter = 0;

        $request->user()->tokens->each( function( $token,$key) use( &$revoked_token_counter)
        {
            $deleted = $token->delete();

            if($deleted)
            {
                $revoked_token_counter++;
            }
        }); */
        Auth::user()->tokens()->delete();

        //$revoked = $request->user()->token()->revoke();
        /*
        if($revoked_token_counter > 0)
        {
            return response()->json(
                [
                    'result' => 'Utente sloggato con successo',
                    'code' =>JsonResponse::HTTP_OK,
                ],
                JsonResponse::HTTP_OK,
            );
        }
        else
        {
            return response()->json(
                [
                    'result' => 'errore nel processo di logout',
                    'code' =>JsonResponse::HTTP_UNPROCESSABLE_ENTITY,
                ],
                JsonResponse::HTTP_UNPROCESSABLE_ENTITY,
            );
        } */

    }

    public function myProfile()
    {
        $user = Auth::user();
        $user->load('details');

        $result =
        [
            'userData'      => $user,
        ];

        return response()->json(
            [
                'result' => $result,
                'code' =>JsonResponse::HTTP_OK,
            ],
            JsonResponse::HTTP_OK,
        );
    }
}
