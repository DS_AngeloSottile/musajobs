<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateJobOfferRequest;
use App\Http\Requests\JobOffersBySkillRequest;
use App\Models\JobOffer;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class JobOffersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateJobOfferRequest $request)
    {

        try
        {
            DB::beginTransaction();
            $now = Carbon::now();


            $job_offer = JobOffer::create($request->all(),[ 'status' => 1 ]);

            $job_offer->statusHistory()->attach(1,[ 'last' => true, 'from' => $now, 'to' => null] );

            $job_offer_skills = $request->get('skills');

            foreach($job_offer_skills as $job_offer_skill)
            {
                $job_offer_skill_extras =
                [
                    'min_level'             => $job_offer_skill['min_level'],
                    'max_level'             => $job_offer_skill['max_level'],
                    'min_experience_years'  => $job_offer_skill['min_years'],
                ];
                $job_offer->skills()->attach($job_offer_skill['id'], $job_offer_skill_extras );
            }


            DB::commit();

            return response()->json( [ 'result' => 'offerta di lavoro creata correttamente', 'code' => JsonResponse::HTTP_CREATED], JsonResponse::HTTP_CREATED );
        }
        catch(\Exception $e)
        {
            DB::rollback();

            return response()->json(['erros' => true, 'message' => 'errore di creazione', 'code' => JsonResponse::HTTP_INTERNAL_SERVER_ERROR ], JsonResponse::HTTP_INTERNAL_SERVER_ERROR );
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    public function jobOffersBySkill(JobOffersBySkillRequest $request)
    {
        $skills_input = $request->get('skills');

        $job_offers_query = JobOffer::with(
            [
                'company',
                'skills',
            ]);

            foreach($skills_input as $skill)
            {
                $job_offers_query->whereHas( 'skills',
                function($q) use ($skill)
                {
                    $q->where('skill_id',$skill);
                    $q->where('min_level','>=',$skill['min_level']);
                    $q->where('max_level','<=',$skill['max_level']);
                    $q->where('min_experience_years','>=',$skill['min_exp']);
                });
            }

            $job_offer = $job_offers_query->get();

        return response()->json( ['result' => $job_offer, 'code' => JsonResponse::HTTP_OK ],JsonResponse::HTTP_OK );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
