<?php

namespace App\Http\Controllers;

use App\Http\Requests\CompanyLogoRequest;
use App\Models\Category;
use App\Models\Company;
use App\Traits\ApiTrait;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CompaniesController extends Controller
{
    use ApiTrait;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $companies = Company::paginate(5);

        return response()->json( [ 'result' => $companies, 'code' => 200], 200 );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $company = Company::findOrFail( $id );
        return response()->json( [ 'result' => $company, 'code' => 200], 200 );
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    public function updLoadLogo(CompanyLogoRequest $request)
    {
        $user = Auth::user();

        $company_logo = 'companies/default.png';

        $company = $user->details;

        if($company &&  $request->hasFile( 'logo' ))
        {
            $file = $request->file( 'logo' );
            $filename = time(). '.' . $file->getClientOriginalExtension(); // mette un nome generico al file
            $file_path = 'companies/' . $company->id . '/logo';
            $uploaded_file = $file->storeAs( $file_path,$filename,'public');
            $company->logo= $uploaded_file; // php artisan storage:link
            $company->save();

            return $this->successResponse(['logo_url' =>$company->company_logo_full_url],JsonResponse::HTTP_CREATED);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function allCategories()
    {
        $categories = Category::get()->all();
        return response()->json( [ 'result' => $categories, 'code' => 200], 200 );
    }
}
