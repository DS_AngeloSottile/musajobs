<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;

class CreateJobOfferRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'role'                      => 'required|string|max:255',
            'description'               => 'required|string',
            'company_id'                => 'required|exists:companies,id',
            'valid_from'                => 'required|date|after_or_equal:now|date_format:Y-m-d H:i',
            'valid_to'                  => 'required|date|after:valid_from|date_format:Y-m-d H:i',
            'skills'                    => 'required|array|min:1',
            'skills.*.id'               => 'required|exists:skills,id|distinct', //distinct non pioi inserire gli stessi valori
            'skills.*.min_level'        => 'required|in:1,1.5,2,2.5,3,3.5,4,4.5,5',
            'skills.*.max_level'        => 'required|in:1,1.5,2,2.5,3,3.5,4,4.5,5|gte:skills.*.min_level',
            'skills.*.min_years'        => 'required|integer|between:1,10',
        ];
    }

    public function messages()
    {
        return
        [
            'required' => ':attribute e richiesto',
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        if($this->expectsJson())
        {
            $errors = (new ValidationException($validator))->errors();
            throw new HttpResponseException( response()->json(
                                                            [
                                                                'erros' => true,
                                                                'error_message' => $errors,
                                                                'code' => JsonResponse::HTTP_UNPROCESSABLE_ENTITY,
                                                            ],
                                                            JsonResponse::HTTP_UNPROCESSABLE_ENTITY ) );
        }
        else
        {
            return parent::failedValidation($validator);
        }
    }
}
