<?php

namespace App\Http\Requests;

use App\Traits\ApiTrait;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;

class CompanyRegistrationRequest extends FormRequest
{
    use ApiTrait;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return
        [
            'first_name'        => 'required|max:255',
            'last_name'         => 'required|max:255',
            'company_name'      => 'required|max:255|min:3',
            'email'             => 'required|max:255|email|unique:users,email',
            'password'          => 'required|max:255|min:8|alpha_num',
            'confirm_password'  => 'required|same:password',
            'category'          => 'required|integer|exist:categories,id'
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        if($this->expectsJson())
        {
            $errors = (new ValidationException($validator))->errors();
            throw new HttpResponseException($this->errorResponse($errors, JsonResponse::HTTP_UNPROCESSABLE_ENTITY));
        }
        else
        {
            return parent::failedValidation($validator);
        }
    }
}
