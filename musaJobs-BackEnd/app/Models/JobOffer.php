<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class JobOffer extends Model
{
    protected $fillable =
    [
        'role',
        'description',
        'status',
        'company_id',
        'valid_from',
        'valid_to',
    ];

    public function company()
    {
        return $this->belongsTo('App\Models\Company','company_id','id');
    }
    public function skills()
    {
    return $this->belongsToMany('App\Models\Skill','job_offer_skill','job_offer_id','skill_id');
    }

    public function statusHistory()
    {
        return $this->belongsToMany('App\Models\JobOfferStatus','job_offer_has_status','job_offer_id','job_offer_status_id')
        ->withPivot(['last','from','to']);
    }


}
