<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class skill extends Model
{
   protected $filalble =
   [
       'label',
       'code'
   ];

   public function users()
   {
       return $this->belongsToMany('App\Models\UserDetails','skill_user','skill_id','user_id');
   }


   public function jobOffers()
   {
   return $this->belongsToMany('App\Models\JobOffer','job_offer_skill','job_offer_id','skill_id');
   }
}
