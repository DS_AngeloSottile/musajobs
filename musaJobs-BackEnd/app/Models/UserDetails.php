<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserDetails extends Model
{
    use SoftDeletes;

    protected $fillable =
    [

        'bio',
        'birth_date',
        'phone',
        'gender',
        'city',
    ];

    public function user()
    {
        return $this->morphOne('App\Models\User','details'); //modello relazionato --- e il nome della relazione(details è quella che hai in user)
    }

    public function skills()
    {
    return $this->belongsToMany('App\Models\Skill','skill_user','user_id','skill_id')->withPivot(['level','experience_year']);
    }

    public function jobOffers()
    {
        return $this->belongsToMany('App\Models\JobOffer','job_offers_users','user_id','job_offer_id');
    }
}
