<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Company extends Model
{
    use SoftDeletes;
    protected $fillable =
    [

        'name',
        'description',
        'city',
        'website',
        'logo',
        'category_id',
    ];

    protected $appends =
    [
        'company_logo_full_url',
    ];

    public function getCompanyLogoFullUrlAttribute()
    {
        return env('APP_URL') . 'storage/' . $this->logo;
    }

    public function jobOffers()
    {
        return $this->hasMany('App\Models\JobOffer','company_id','id');
    }
    public function user()
    {
        return $this->morphOne('App\Models\User','details'); //modello relazionato --- e il nome della relazione(details è quella che hai in user)
    }

    public function category()
    {
        return $this->belongsTo('App\Models\Category','category_id','id');
    }
}
