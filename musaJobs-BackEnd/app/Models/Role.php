<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
        /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'label',
        'code',
    ];

    public function users()
    {
        return $this->hasMany('App\Models\Users','role_id','id');
    }
}
