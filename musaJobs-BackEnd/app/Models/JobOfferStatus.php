<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class JobOfferStatus extends Model
{
    protected $table = 'job_offer_status';
    protected $filliable=
    [
        'label',
        'code',
        'created_at',
        'updated_at',
    ];

    public function jobOffers()
    {
        return $this->belongsToMany('App\Models\JobOffer','job_offer_has_status','job_offer_status_id','job_offer_id');
    }

    public function scopeLast($query)
    {
        return $query->where('last', true);
    }

    public function scopeOrderByStatusHistory($query)
    {
        $query->orderBy('from','DESC');
        $query->orderBy('to','ASC');
        return $query;
    }
}
