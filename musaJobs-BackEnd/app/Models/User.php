<?php

namespace App\Models; // devi andare in config - auth e cambiare

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use Notifiable, SoftDeletes, HasApiTokens; //sulla documentazione trovi tutto per passport

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'f.irst_name',
        'last_name',
        'email',
        'password',
        'username',
        'privacy',
        'role_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public function getFullnameAttribute() //Fullname è il nome di questa funzione l'hai richiamata in app.blade.php per scrivere il tuo nome
    {
        return $this->first_name. ' ' .$this->last_name;
    }

    public function Role()
    {
        return $this->belongsTo('App\Models\Role','role_id','id');
    }

    public function details()
    {
        return $this->morphTo();
    }

    public function scopeActive($query)
    {
        return $query->whereNotNull('email_verified_at');
    }

    public function scopeNotActive($query)
    {
        return $query->where('email_verified_at',null);
    }

    public function scopeStatus($query,$status)
    {
        if($status === true)
        {
            return $query->whereNotNull('email_verified_at' );
        }
        else
        {
            return $query->whereNull('email_verified_at');
        }
    }

    public function scopebyRole($query, $role_id)
    {
        return $query->where( 'role_id', $role_id );
    }

    public function hasRole( $role)
    {
        return $this->role->code === $role;
    }

    public function scopeByType($query,$type)
    {
        if($type === 'user')
        {
            $query->where('details_type','App\Models\UserDetails');
        }
        else if ($type === 'company')
        {
            $query->where('details_type','App\Models\Company');
        }
        else if($type === 'admin')
        {
            $query->whereNull('details_type');
        }


        return $query;
    }

    public function scopeOrdered( $query, $order)
    {
        if( !is_null($order))
        {
            if( $order === 'asc')
            {
                $query->orderBy('last_name','asc');
            }
            else if( $order === 'desc')
            {
                $query->orderBy('last_name','desc');
            }
            $query->orderBy('first_name','asc');
        }
    }

    protected static function boot() // secondo modo degli observer automaticamente quando un utente viene acncellato o ripristinato si attiveranno queste qua
    {
        parent::boot();

        static::deleting(function($model)
        {
            if($model->details_type === 'App\Models\UserDetails')
            {
                if($model->details)
                {
                    $model->details->jobOffers()->detach(); // detach per le belongs to many
                }
                $model->details()->delete();
            }
            else if ( $model->details_type == 'App\Models\Company')
            {
                $model->details->jobOffers()->delete();
                $model->details()->delete();
            }
        });

        static::restoring(function($model)
        {
            if($model->details_type === 'App\Models\UserDetails')
            {
                $model->details()->restore();
            }
            else if ( $model->details_type == 'App\Models\Company')
            {
                $model->details()->restore();
            }
        });

    }
}
