<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{

    protected $fillable =
    [
        'label',
        'code',
    ];

    public function companies()
    {
        return $this->hasMany('App\Models\Company','category_id','id');
    }

    public function scopeOrdered ($query)
    {
        return $query->orderBy('label','ASC');
    }
}
