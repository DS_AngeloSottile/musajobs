import axios from "axios";

class ApiServices
{
    uploadUserPhoto( file,onUploadProgress )
    {
        const formData = new FormData();
        formData.append('logo',file); // nome che si aspetta l'API e dove hai messo la foto

        return axios.post( 'upload-logo',formData, { onUploadProgress } );
    }

    manageSkill( data )
    {
        return axios.post('user-skill-add',data);
    }

    removeSkill( id )
    {
        return axios.post('user-skill-remove',{skill_id : id});
    }
    

}

export default new ApiServices();