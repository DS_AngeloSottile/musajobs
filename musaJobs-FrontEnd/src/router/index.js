import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import store from '../store'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/prova',
    name: 'Prova',
    component: () => import( '../views/Prova.vue'),
  },
  {
    path: '/about',
    name: 'About',
    component: () => import( '../views/About.vue')
  },
  {
    path: '/register-company',
    name: 'RegisterCompany',
    component: () => import( '../views/RegisterCompany.vue'),
    beforeEnter: (to,from,next) =>
    {
      if( store.getters.isAuth )
      {
        next('/login');
      }
      else
      {
        next();
      }
    }
  },
  {
    path: '/profile',
    name: 'profile',
    component: () => import( '../views/Profile.vue'),
    beforeEnter: (to,from,next) =>
    {
      if( store.getters.isAuth )
      {
        next();
      }
      else
      {
        next('/login');
      }
    }
  },
  {
    path: '/login',
    name: 'Login',
    component: () => import( '../views/Login.vue'),

    children: 
    [
      {
        path: 'user',
        name: 'UserLogin',
        component: () => import( '../views/UserLogin.vue'),
      },
      {
        path: 'company',
        name: 'CompanyLogin',
        component: () => import( '../views/CompanyLogin.vue'),
      }
    ],
    beforeEnter: (to,from,next) =>
    {
      if( store.getters.isAuth )
      {
        next('/list');
      }
      else
      {
        next();
      }
    }
  },
  {
    path: '/inputs',
    name: 'Inputs',
    
    component: () => import( '../views/Inputs.vue')
  },
  {
    path: '/register',
    name: 'Register',
    
    component: () => import( '../views/Register.vue'),
    beforeEnter: (to,from,next) =>
    {
      if( store.getters.isAuth )
      {
        next('/list');
      }
      else
      {
        next();
      }
    }
  },
  {
    path: '/list',
    name: 'List',
    
    component: () => import( '../views/List.vue'),
    beforeEnter: (to,from,next) =>
    {
      if( store.getters.isAuth )
      {
        next();
      }
      else
      {
        next('/login');
      }
    }
  },
  {
    path: '/404',
    name: 'notFound',
    component: () => import( '../views/404.vue'),
  },
  {path: '*', redirect:'/404'}
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
