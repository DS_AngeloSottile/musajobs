import axios from 'axios';
const state = 
{
    skills: null,
    jobOffers: [],
    jobOffersMaxPages: 1,
    jobOffersCurrentPage: 1,
};

const getters = 
{
    skills: state =>
    {
        return state.skills;
    },
    jobOffers: state =>
    {
        return state.jobOffers;
    },
    jobOffersMaxPages: state =>
    {
        return state.jobOffersMaxPages;
    },
    jobOffersCurrentPage: state =>
    {
        return state.jobOffersCurrentPage;
    },
};

const mutations =   // QUESTA MUTATIONS SERVE SOLO PER SETTARE LE VARAIBILI, ogni volta che aggiungi qualcosa tramite api dev'esse richiamato lo store, funziona anche senza ma dovresti sempre refreshare la pagina
{
    setSkills: ( state,payload) =>
    {
        state.skills = payload; // le skill verranno messe nello state così dall'essere visualizzate immediatamente senza dover refreshare la pagina 
    },
    setJobOffers: ( state,payload) =>
    {
        state.jobOffers = [...state.jobOffers,...payload.data.result.data]; 
        state.jobOffersMaxPages = payload.data.result.last_page;
        state.jobOffersCurrentPage = payload.data.result.current_page;
    },
    resetJobOffers: (state) =>
    {
        state.jobOffers = [];
        state.jobOffersMaxPages = 1;
        state.jobOffersCurrentPage = 1;
    }
};

const actions =
{
    fetchUserSkills: ( {commit} ) =>
    {
        axios
        .post('my-skills')
        .then(res => 
            {
                commit('setSkills', res.data.result);
            })
        .catch(err =>
            {
                console.log(err);
            });

    },

    refreshUserSkills: ( {commit}, payload ) => // payload sono le skill passate così res.data.result.user_skills
    {
        commit('setSkills', payload); //chiamo la mutation setSkills passando le skill
    },
    
    searchJobOffers: ( {commit} , payload) =>
    {
        axios
            .post('job-offers-by-skill', payload.skills,
            {
                params:
                {
                    page: payload.page,
                }
            })
            .then( (res) =>
            {
                console.log('res job offers',res);
                commit('setJobOffers', res);
            })
            .catch( (err) =>
            {
                console.log(err);
            });
    },
    resetJobOffers: ( {commit} ) =>
    {
        commit('resetJobOffers');
    }
};


export default {
    state,
    getters,
    mutations,
    actions
};