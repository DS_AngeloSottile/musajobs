import axios from 'axios';
import router from '../../router'
const state = 
{
    token: localStorage.getItem('token') || null,
    userData:  {}, // invece di valorizzare qua userData, lo valorizzi nell'App richiamando il profilo dell'utente
    errors : {},
};
const getters = 
{
    token: state => 
    {
        return state.token;
    },
    user: state => 
    {
        return state.userData;
    },
    isAuth: state =>
    {
        return state.token !== null;
    },
    errors: state =>
    {
        return state.errors;
    },
    userType: state =>
    {
        switch( state.userData.role_id )
        {
            case 2:
                return 'user';
            case 3:
                return 'company';
            default:
                return 'none';
        }
    },
    isUser: state =>
    {
        return state.userData.role_id === 2;
    },
    isCompany: state =>
    {
        return state.userData.role_id === 3;
    }
};
const mutations = {
    login: ( state, payload ) =>
    {
        console.log('entra login',payload);
        state.userData = payload.userData;
        state.token = payload.token; /* il valore token di qua è il valore token che hai quando ti loggi  tramite questo
                                                            axios.post('user-login',postBody) // user login è la rotta
                                                            .then( res =>
                                                            {
                                                            console.log( res.data.result );
                                                            })
                                                            .catch( err =>
                                                            {
                                                            console.log(err.data);
                                                            }); */
        localStorage.setItem('token',payload.token);
        router.push('/list');
    },
    logout: (state) =>
    {
        state.userData = {};
        state.token = null;
        localStorage.removeItem('token');
        router.push('/');
    },
    setUserData: ( state, payload ) => // questo ti serve solo per memorizzare i dati dell'utente, poichè non funziona con localstorage
    {
        state.userData = payload;
    },
    setErrors: (state,payload) =>
    {
        state.errors = payload;
    },
    resetErrors: (state) =>
    {
        state.errors = {};
    },
    setUserAvatar: (state,payload) =>
    {
        console.log('mutations',payload);
        console.log('user',state.userData.details);
        state.userData.details.logo = payload;
        state.userData.details.company_logo_full_url = payload;
    },
};
const actions = 
{  //da login.vue richiama il doLogin, che deve richiamare il back-end tramite axios, se lo richiama allora fa il then che chiama il login di vuejs
    doLogin: ({commit},payload) =>
    { //tramite axios si contata il back-end
        axios
        .post('user-login',payload) // user login è la rotta laravel
        .then( res =>
        {
            commit('login',res.data.result); // il login è la funzione che hai in auth.js mentre re.data.result è quello che ti spunta quando fai il console.log
            console.log('user', res.data.result);
        })
        .catch( err =>
        {
            console.log(err.data);
        });
    },
    doCompanyLogin: ({commit},payload) =>
    { 
        axios
        .post('company-login',payload) 
        .then( res =>
        {
            commit('login',res.data.result); 
        })
        .catch( err =>
        {
            console.log(err.data);
        });
    },
    doLogout: ({commit}) =>
    {
        axios
            .post('logout') //il logout è la tua rotta di laravel
            .then( (res) =>
            {
                if(res.status === 200)
                {
                    console.log(res);
                    commit('logout');
                }
                else
                {
                    throw new Error('errore di Logout');
                }
            })
            .catch( (err) =>
            {
                commit('logout');
                console.log(err);
            });
    },
    checkLogin: ({commit}) =>
    {
        if(!state.token) // se è la prima volta che visiti la pagina non ha senso richiamare il back-end, quindi ritorna subito 
        {
            return;
        }
        axios
        .post( 'my-profile' )
        .then( (res) =>
        {
            console.log('myprofile',res);
            if(res.status === 200)
            {
                commit('setUserData', res.data.result.userData);
            }
        })
        .catch( () => //può essere che il token dia errore, quindi se c'è un errore chiami il logout che elimina il token e i dati dell'utente
        {
            commit('logout');
        });
    },
    registerCompany: ({commit}, payload) =>
    {
        const postBody =
        {   
            first_name: payload.firstName,
            email: payload.email,
            last_name: payload.lastName,
            category: payload.category,
            company_name: payload.companyName,
            password: payload.password,
            confirm_password: payload.passwordConfirm,
            privacy: payload.privacy,
        }
        commit('resetErrors'); // in questo modo quando parte la chiamate svuoti gli errori
        axios
        .post('register-company',postBody) // la query che registra nel database è su laravel, se va abuon fine...
        .then( (res) =>
        {
            commit('setUserData',postBody);
            commit('login', res.data.result); // ...ti auto logga 
        })
        .catch( (err) =>
        {
            commit('setErrors',err.response.data.message);
            // console.log(err);
            // console.log(err.request);
            // console.log(err.response);
            // console.log(err.message);
            // console.log(err.config);
        });
    },
    registerUser: ({commit}, payload) =>
    {
        const postBody =
        {   
            first_name: payload.firstName,
            email: payload.email,
            last_name: payload.lastName,
            password: payload.password,
            confirm_password: payload.passwordConfirm,
            privacy: payload.privacy,
        }
        commit('resetErrors'); // in questo modo quando parte la chiamate svuoti gli errori
        axios
        .post('register-user',postBody) // la query che registra nel database è su laravel, se va abuon fine...
        .then( (res) =>
        {
            commit('login', res.data.result); // ...ti auto logga, l'auto login non funziona, non hai il payload 
        })
        .catch( (err) =>
        {
            commit('setErrors',err.response.data.message);
            //console.log(err);
            // console.log(err.request);
            // console.log(err.response);
            // console.log(err.message);
            // console.log(err.config);
        });
    },
    setErrors: ({commit}, payload) =>
    {
        commit('setErrors',payload.response.data.message);
    },
    resetErrors: ( {commit} ) =>
    {
        commit('resetErrors');
    },

    setUserAvatar: ( {commit}, payload) =>
    {
        console.log('actions',payload);
        commit('setUserAvatar',payload);
    },
};


export default {
    state,
    getters,
    mutations,
    actions
};