module.exports = {
  purge: [],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      colors:
      {
        'mj':
        {
          'green-1': '#7bca55',
          'blue-1': '#1a68b3',
        },
      },
    },
  },
  variants: {
    backgroundColor: ['responsive', 'disabled', 'hover', 'focus'],
    extend: {},
  },
  plugins: [],
}
